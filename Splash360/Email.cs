﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splash360
{
    public class Email
    {
        IWebDriver driver;

        public Email(IWebDriver webDriver)
        {
            driver = webDriver;
        }

        public void GoToTab()
        {
            //IWebElement emailTab = driver.FindElement(By.XPath("/html/body/table/tbody/tr/td/div/div/div[2]/div/div[5]"));
            IWebElement emailTab = driver.FindElement(By.Id("email"));
            emailTab.Click();
            
            IWebElement tempGoEmail = driver.FindElement(By.Id("EmailTemplateHolderMain"));
            if (tempGoEmail.Displayed == true)
            {
                TestResultFile.SaveResult("Email Tab Successfully Viewed");
            }
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));
        }

        public void SendEmail()
        {
            IWebElement emailComposeBtn = driver.FindElement(By.Id("emailCompose"));
            emailComposeBtn.Click();

            IWebElement emailTo = driver.FindElement(By.Id("to"));
            emailTo.SendKeys("milon@bs-23.com");

            IWebElement emailSubject = driver.FindElement(By.Id("subject"));
            emailSubject.SendKeys("Test Mail");
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));

            IWebElement writeBody = driver.FindElement(By.XPath("/html/body"));
            writeBody.Click();
            writeBody.SendKeys("Hello !!");

            IWebElement sendMailbtn = driver.FindElement(By.XPath("/html/body/table/tbody/tr[2]/td/div[3]/table/tbody/tr/td/table/tbody/tr/td/div/table/tbody/tr[17]/td/div/span/span[2]/span/table/tbody/tr/td/div/span[9]/span[2]/span/a/span[2]"));
            sendMailbtn.Click();
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));
        }

        public void SendMultipleEmail()
        {
            StringBuilder sb = new StringBuilder();         
            IWebElement emailComposeBtn = driver.FindElement(By.Id("emailCompose"));
            emailComposeBtn.Click();

            IWebElement emailTo = driver.FindElement(By.Id("to"));
            int totalEmail = 10;
            for (int i = 1; i <= totalEmail; i++)
            {
                sb.Append("milon+" + i + "@bs-23.com");
                if (i < totalEmail)
                {
                    sb.Append(",");
                }
            }
            emailTo.SendKeys(sb.ToString());
            IWebElement emailSubject = driver.FindElement(By.Id("subject"));
            emailSubject.SendKeys("Test Mail");
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));

            IWebElement writeBody = driver.FindElement(By.XPath("/html/body"));
            writeBody.Click();
            writeBody.SendKeys("Hello !!");

            IWebElement sendMailbtn = driver.FindElement(By.XPath("/html/body/table/tbody/tr[2]/td/div[3]/table/tbody/tr/td/table/tbody/tr/td/div/table/tbody/tr[17]/td/div/span/span[2]/span/table/tbody/tr/td/div/span[9]/span[2]/span/a/span[2]"));            
            sendMailbtn.Click();
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));
        }

        public void SendCampaign()
        {
            IWebElement emailComposeBtn = driver.FindElement(By.Id("emailCompose"));
            emailComposeBtn.Click();

            IWebElement emailTo = driver.FindElement(By.Id("lnkEmailTo"));
            emailTo.Click();

            IWebElement allContact = driver.FindElement(By.XPath("/html/body/ul[2]/li/a/span"));
            allContact.Click();

            IWebElement emailSubject = driver.FindElement(By.Id("subject"));
            emailSubject.SendKeys("Test Campaign");
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));

            IWebElement writeBody = driver.FindElement(By.XPath("/html/body"));
            writeBody.Click();
            writeBody.SendKeys("Hello !! this Is Test Campaign.");

            // The following 4 lines are for schedule campaign
            IWebElement scheduleClick = driver.FindElement(By.Id("cke_138_label"));
            scheduleClick.Click();
            IWebElement scheduleSave = driver.FindElement(By.Id("schedule_now"));
            scheduleSave.Click();

            // The following 4 lines are for normal campaign
            //IWebElement sendMailbtn = driver.FindElement(By.XPath("/html/body/table/tbody/tr[2]/td/div[3]/table/tbody/tr/td/table/tbody/tr/td/div/table/tbody/tr[17]/td/div/span/span[2]/span/table/tbody/tr/td/div/span[9]/span[2]/span/a/span[2]"));
            //sendMailbtn.Click();
            //IWebElement sendNow = driver.FindElement(By.Id("deliver_send_now"));
            //sendNow.Click();       
            
            
        }
    }
}
