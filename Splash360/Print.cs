﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splash360
{
    class Print
    {
        IWebDriver driver;

        public Print(IWebDriver webDriver)
        {
            driver = webDriver;
        }

        public void GoToTab()
        {            
            IWebElement printTab = driver.FindElement(By.Id("print"));
            printTab.Click();

            IWebElement tempGoPrint = driver.FindElement(By.Id("PrintTemplateBody"));
            if (tempGoPrint.Displayed == true)
            {
                TestResultFile.SaveResult("Print Tab Successfully Viewed");
            }
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));
        }

        public void GotToDraft()
        {
            IWebElement clickDropdown = driver.FindElement(By.ClassName("headerIcon"));
            clickDropdown.Click();

            //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(10));

            IWebElement clickDraft = driver.FindElement(By.Id("splash_panelItem_265"));
            clickDraft.Click();
        }
    }
}
