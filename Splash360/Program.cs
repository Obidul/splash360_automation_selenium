﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox; // This is for FF driver
using System.IO;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.IE;// This is for IE driver
using OpenQA.Selenium.Chrome; // This is for Chrome driver

namespace Splash360
{
    class Program
    {
        static void Main(string[] args)
        {
            //This code is for Initialization of the FF driver
            IWebDriver driver = new FirefoxDriver();

            // This code is for Initialization the IE Driver
            //IWebDriver driver = new InternetExplorerDriver(@"C:\Libraries\");

            // This code is for Initialization the Chrome Driver
            //IWebDriver driver = new ChromeDriver(@"C:\Libraries\");            
            
            // Maximize the open window
            driver.Manage().Window.Maximize();

            //Register register = new Register(driver);
            //register.registrationProcess();           
            //register.checkMail();
           
            // Do login to the application
            LogIn login = new LogIn(driver);
            login.DoLogIn();

            // Check Subscription
            //Subscription sub = new Subscription(driver);
            //sub.CheckSubs();
            //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(15));                 
            
            // Click on email tab
            Email email = new Email(driver);
            email.GoToTab();

            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(15));
            //Send Email Campaign to ALL Contacts As a group
            //email.SendCampaign();

            //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(30));

            // Only Send One mail to the specific email address
            //email.SendEmail();

            // Send Multiple email at the same time in the to field
            //email.SendMultipleEmail();

            //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(15));

            // Click on Social tab
            //Social social = new Social(driver);
            //social.GoToTab();

            //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(15));
            // This Function will post on the Social pages.
            //social.PostMsg();

            // This Function will post schedule msg on the Social pages.
            //social.SchePost();

            // Click on Print tab
            //Print print = new Print(driver);
            //print.GoToTab();

            //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(10));
            //print.GotToDraft();

            //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(15));

            // Click on Contacts tab
            //Contacts contact = new Contacts(driver);
            //contact.GoToTab();

            //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(15));
            //contact.AddUser();
            //contact.ImpContact();

            //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(15));

            // Click on Reports tab
            //Reports report = new Reports(driver);
            //report.GoToTab();

            //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(10));
            // Will close the web driver window
            //driver.Close();
        }
    }
}
