﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splash360
{
    class Register
    {
        IWebDriver driver;
        string eAdd = "milon+1007@bs-23.com", uName = "milon1007", uPass = "milon1007";

        public Register(IWebDriver webDriver)
        {
            driver = webDriver;
        }

        public void registrationProcess()
        {
            driver.Navigate().GoToUrl("https://preprod.splash360.com");

            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(30));

            IWebElement freeReg = driver.FindElement(By.XPath("/html/body/div[2]/div/div[3]/a"));
            freeReg.Click();

            IWebElement emailAdd = driver.FindElement(By.Id("EmailAddress"));
            emailAdd.SendKeys(eAdd);

            IWebElement userName = driver.FindElement(By.Id("UserName"));
            userName.SendKeys(uName);

            IWebElement userPass = driver.FindElement(By.Id("Password"));
            userPass.SendKeys(uPass);

            IWebElement createAccount = driver.FindElement(By.XPath("/html/body/div[2]/div/div[2]/div[2]/div[2]/div/form/div/div[4]/span"));
            createAccount.Click();            
        }

        public void checkMail()
        {   
            //Open new tab with the existing window
            IWebElement newTab = driver.FindElement(By.TagName("body"));
            newTab.SendKeys(Keys.Control + 't');

            driver.Navigate().GoToUrl("https://www.gmail.com");            
            //Wait untill page is loaded
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(10));

            // Write email address in the email filed
            IWebElement emailName = driver.FindElement(By.Id("Email"));
            emailName.Clear(); // clearing the email field before fill up
            emailName.SendKeys("milon@bs-23.com");

            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(1)); // wait 1 second

            // Write password in the password filed
            IWebElement emailPass = driver.FindElement(By.Id("Passwd"));
            emailPass.Clear(); // clearing the passowrd field before fill up
            emailPass.SendKeys("obidul23");

            // Click on the sign in button
            IWebElement sIn = driver.FindElement(By.Id("signIn"));
            sIn.Click();

            // wait 30 sec to load the email inbox page
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(30));

            // identify the search field , write the searching data and click on the search button
            IWebElement searchEmail = driver.FindElement(By.XPath("/html/body/div[5]/div[2]/div/div/div[3]/div/div/div[3]/div/div/div/form/fieldset[2]/div/div/div[2]/input"));
            searchEmail.Click();
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(3));
            searchEmail.SendKeys("subject:(Welcome to Splash360)");
            IWebElement searchBth = driver.FindElement(By.XPath("/html/body/div[5]/div[2]/div/div/div[3]/div/div/div[3]/div/div/div/form/div/button"));
            searchBth.Click();
            
            // wait 3 seconds before the search result appear
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(3));

            // click on the top email which is found by the search
            IWebElement clickMail = driver.FindElement(By.XPath("/html/body/div[5]/div[2]/div/div[2]/div/div[2]/div/div/div/div[2]/div/div/div/div[2]/div[6]/div/div/table/tbody/tr/td[6]"));
            clickMail.Click();

            // The following 3 line code will write the whole email body on the console
            //IWebElement mailBody = driver.FindElement(By.ClassName("a3s"));
            //Console.WriteLine(mailBody.Text);
            //Console.Read();

            // Read the searched body, find the username as registered and show msg that the username is found or not.
            IWebElement mailBody = driver.FindElement(By.ClassName("a3s"));
            string emailContent = mailBody.Text;
            if (emailContent.Contains(uName))
            {
                Console.WriteLine("Found");
            }
            else {
                Console.WriteLine("Not Found");
            }            
            if (emailContent.Contains("Activate my account"))
            {
                Console.WriteLine("Activating Email");
                IWebElement clickActivate = driver.FindElement(By.LinkText("Activate my account"));                
                clickActivate.Click();
            }
            //Console.Read();
        }

        public void loginAfterValidaton()
        {

        }
    }
}
