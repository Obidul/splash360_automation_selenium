﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splash360
{
    class Social
    {
        IWebDriver driver;

        public Social(IWebDriver webDriver)
        {
            driver = webDriver;
        }

        public void GoToTab()
        {            
            IWebElement socialTab = driver.FindElement(By.Id("social"));
            socialTab.Click();

            IWebElement tempGoSocial = driver.FindElement(By.ClassName("socialEditorContainerTd"));
            if (tempGoSocial.Displayed == true)
            {
                TestResultFile.SaveResult("Social Tab Successfully Viewed");
            }
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));
        }

        public void PostMsg()
        {   
            IWebElement selectFb = driver.FindElement(By.Id("newMessageFacebook_btn"));
            selectFb.Click();

            IWebElement selectTw = driver.FindElement(By.Id("newMessageTwitter_btn"));
            selectTw.Click();

            IWebElement selectLi = driver.FindElement(By.Id("newMessageLinkedin_btn"));
            selectLi.Click();

            IWebElement writeMsg = driver.FindElement(By.Id("social_txt"));
            writeMsg.Click();
            writeMsg.SendKeys("Hello !! How Are You :)");

            //IWebElement ClickSendBtn = driver.FindElement(By.XPath("/html/body/table/tbody/tr[2]/td/div[3]/table/tbody/tr/td/table/tbody/tr/td/div/table/tbody/tr[17]/td/div/div/table/tbody/tr/td/table/tbody/tr/td/div/div/div[3]/div/div[3]/div[2]/div/input[2]"));
            IWebElement clickSendBtn = driver.FindElement(By.Id("SocailMessagePost_btn"));
            clickSendBtn.Click();
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));
        }

        public void SchePost()
        {
            IWebElement selectFb = driver.FindElement(By.Id("newMessageFacebook_btn"));
            selectFb.Click();

            IWebElement selectTw = driver.FindElement(By.Id("newMessageTwitter_btn"));
            selectTw.Click();

            IWebElement selectLi = driver.FindElement(By.Id("newMessageLinkedin_btn"));
            selectLi.Click();

            IWebElement clickScheBtn = driver.FindElement(By.Id("SocialHistory"));
            clickScheBtn.Click();

            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));

            IWebElement writeMsg = driver.FindElement(By.Id("social_txt"));
            writeMsg.Click();
            writeMsg.SendKeys("Have a nice day)");

            IWebElement savePostBtn = driver.FindElement(By.Id("SocailScheduledMessagePost_btn"));
            savePostBtn.Click();
        }
    }
}
