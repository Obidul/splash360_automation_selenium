﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splash360
{
    public class Subscription
    {
        IWebDriver driver;

        public Subscription(IWebDriver webDriver)
        {
            driver = webDriver;
        }
        public void CheckSubs()
        {
            IWebElement ClickProfile = driver.FindElement(By.ClassName("headerProfile"));
            //IWebElement ClickProfile = driver.FindElement(By.XPath("/html/body/table/tbody/tr/td/div/div[2]/div"));
            ClickProfile.Click();

            //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(15));
            IWebElement ClickProfileSetting = driver.FindElement(By.ClassName("profilesetting"));
            ClickProfileSetting.Click();

            //driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(60));
            //IWebElement subsCrip = driver.FindElement(By.Id("subscriptionPlan"));
            //subsCrip.Click();

            try
            {
                IWebElement profilePage = driver.FindElement(By.Id("ProfileRight"));
                if (profilePage.Displayed == true)
                {
                    IWebElement subsCrip = driver.FindElement(By.Id("subscriptionPlan"));
                    subsCrip.Click();                   
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            try
            {
                IWebElement wholePage = driver.FindElement(By.ClassName("manageUserTable"));
                if (wholePage.Displayed == true)
                {
                    IWebElement buyNow = driver.FindElement(By.ClassName("buyNow"));
                    buyNow.Click();                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
