﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splash360
{
    public static class TestResultFile
    {
        public static void SaveResult(string Result)
        {
            StreamWriter log;

            if (!File.Exists(@"C:\Users\BS26\Documents\Visual Studio 2012\Projects\Splash360\Splash360\TestResult\Log\logFile.txt"))
            {
                log = new StreamWriter(@"C:\Users\BS26\Documents\Visual Studio 2012\Projects\Splash360\Splash360\TestResult\Log\logFile.txt");
            }
            else
            {
                log = File.AppendText(@"C:\Users\BS26\Documents\Visual Studio 2012\Projects\Splash360\Splash360\TestResult\Log\logFile.txt");
            }
            log.WriteLine(DateTime.Now);
            log.WriteLine("=====================");
            log.WriteLine(Result);
            log.WriteLine();
            log.Close();
        }
    }
}