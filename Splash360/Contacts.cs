﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splash360
{
    public class Contacts
    {
        IWebDriver driver;

        public Contacts(IWebDriver webDriver)
        {
            driver = webDriver;
        }

        public void GoToTab()
        {
            //IWebElement contactTab = driver.FindElement(By.XPath("/html/body/table/tbody/tr/td/div/div/div[2]/div/div[3]"));
            IWebElement contactTab = driver.FindElement(By.Id("addressBook"));
            contactTab.Click();

            IWebElement tempGoContact = driver.FindElement(By.Id("AddressBookLeftContent"));
            if (tempGoContact.Displayed == true)
            {
                TestResultFile.SaveResult("Contacts Tab Successfully Viewed");
            }
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));
        }

        public void AddUser()
        {
            IWebElement addCon = driver.FindElement(By.Id("btnAddContact"));
            addCon.Click();

            IWebElement addEmail = driver.FindElement(By.XPath("/html/body/table/tbody/tr[2]/td/div[4]/table/tbody/tr/td/div/div[4]/div[2]/div/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/div/div/input"));
            addEmail.Click();
            addEmail.SendKeys("fddhdjk@jsaf.com");

            IWebElement clickSave = driver.FindElement(By.XPath("/html/body/table/tbody/tr[2]/td/div[4]/table/tbody/tr/td/div/div[4]/div/div/div[2]/div/div/div[2]"));
            clickSave.Click();
        }

        public void ImpContact()
        {
            IWebElement clickImportLink = driver.FindElement(By.ClassName("importConatct"));
            clickImportLink.Click();

            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));

            IWebElement clkNext = driver.FindElement(By.ClassName("saveButton"));
            clkNext.Click();

            //IWebElement clkBrowse = driver.FindElement(By.ClassName("cancelButton"));
            //clkBrowse.Click();
        }
    }        
}
