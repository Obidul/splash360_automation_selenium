﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splash360
{
    public static class TakeScreenShot
    {
        public static void SaveScreenShot(IWebDriver driver, string ScreenShotFileName)
        {            
            var FolderLocation = @"C:\Users\BS26\Documents\Visual Studio 2012\Projects\Splash360\Splash360\TestResult\ScreenShot\";
            if(!Directory.Exists(FolderLocation))
            {
                Directory.CreateDirectory(FolderLocation);
            }
            var ScreenShot = ((ITakesScreenshot)driver).GetScreenshot();
            var FileName = new StringBuilder(FolderLocation);

            FileName.Append(ScreenShotFileName);
            FileName.Append(DateTime.Now.ToString("dd-mm-yyyy HH_mm_ss"));
            FileName.Append(".png");

            ScreenShot.SaveAsFile(FileName.ToString(),System.Drawing.Imaging.ImageFormat.Png);
        }

        internal static void SaveScreenShot()
        {
            throw new NotImplementedException();
        }
    }
}
