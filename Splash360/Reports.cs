﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splash360
{
    class Reports
    {
        IWebDriver driver;

        public Reports(IWebDriver webDriver)
        {
            driver = webDriver;
        }

        public void GoToTab()
        {
            //IWebElement reportTab = driver.FindElement(By.XPath("/html/body/table/tbody/tr/td/div/div/div[2]/div/div[11]"));
            IWebElement reportTab = driver.FindElement(By.Id("campaign"));
            reportTab.Click();

            IWebElement tempGoReport = driver.FindElement(By.Id("splash_4"));
            if (tempGoReport.Displayed == true)
            {
                TestResultFile.SaveResult("Reports Tab Successfully Viewed");
            }
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(5));
        }
    }
}
