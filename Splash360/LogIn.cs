﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splash360
{
    public class LogIn
    {
        IWebDriver driver;

        public LogIn(IWebDriver webDriver)
        {
            driver = webDriver;
        }

        public void DoLogIn()
        {

            driver.Navigate().GoToUrl("https://preprod.splash360.com/Account/Login");
            //driver.Navigate().GoToUrl("https://qa.splash360.com");

            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(30));        
            IWebElement loginBtn = driver.FindElement(By.Id("login"));
            loginBtn.Click();

            IWebElement userName = driver.FindElement(By.Id("username"));
            userName.SendKeys("milon10");          
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(1));

            IWebElement userPass = driver.FindElement(By.Id("password"));
            userPass.SendKeys("123456");
            //userPass.Submit();
           
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(2));
            IWebElement LoginBut = driver.FindElement(By.Id("login_btn"));        
            LoginBut.Click();

            //Wait until Dashboard fully loaded                                  
            driver.Manage().Timeouts().ImplicitlyWait(System.TimeSpan.FromSeconds(90));            
            
            // After Login page and take the snapshoot and log time in files
            try
            {
                IWebElement tempLogin = driver.FindElement(By.Id("TopPane"));
                if (tempLogin.Displayed == true)
                {
                    TakeScreenShot.SaveScreenShot(driver, "Login: ");
                    TestResultFile.SaveResult("Log In Successfully Completed");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            // Detect Red Banner And Close the banner.
            try
            {
                IWebElement msgWrapper = driver.FindElement(By.ClassName("button_area"));
                if (msgWrapper.Displayed == true)
                {
                    IWebElement redCross = driver.FindElement(By.ClassName("info_close_btn"));
                    redCross.Click();
                    TakeScreenShot.SaveScreenShot(driver, "Login: ");
                    TestResultFile.SaveResult("Log In Successfully Completed");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }            
    }
}
